# Copyright 2022 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require github [ user=Governikus project=${PN}2 release=${PV} suffix=tar.gz ] cmake

SUMMARY="Official authentication app for German ID card and residence permit"
HOMEPAGE+=" https://www.ausweisapp.bund.de/home/"

LICENCES="EUPL-1.2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# fails to build, last checked: 1.22.2
RESTRICT="test"

QT6_MIN_VER="6.4"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:6[>=${QT6_MIN_VER}] [[ note = [ Qt6LinguistTools ] ]]
    build+run:
        net-libs/http-parser[>=2.8.0]
        sys-apps/pcsc-lite
        x11-libs/qtbase:6[>=${QT6_MIN_VER}]
        x11-libs/qtdeclarative:6[>=${QT6_MIN_VER}]
        x11-libs/qtscxml:6[>=${QT6_MIN_VER}]
        x11-libs/qtsvg:6[>=${QT6_MIN_VER}]
        x11-libs/qtwebsockets:6[>=${QT6_MIN_VER}]
        providers:eudev? ( sys-apps/eudev )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.1] )
        providers:systemd? ( sys-apps/systemd )
    suggestion:
        sys-apps/pcsc-cyberjack [[ description = [ Card reader that supports German ID cards ] ]]
"

src_configure() {
    local cmakeparams=(
        -DBUILD_SHARED_LIBS:BOOL=FALSE
        -DBUILD_TESTING:BOOL=$(expecting_tests TRUE FALSE)
        -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen:BOOL=TRUE
        -DCONTAINER_SDK:BOOL=FALSE
        -DINTEGRATED_SDK:BOOL=FALSE
        -DSELFPACKER:BOOL=FALSE
        -DUPDATE_TRANSLATIONS:BOOL=FALSE
        -DUSE_SMARTEID:BOOL=FALSE
    )

    ecmake "${cmakeparams[@]}"
}

