# Copyright 2010-2011 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Copyright 2012-2013 Lasse Brun <bruners@gmail.com>
# Copyright 2013-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mumble-1.2.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2010 Gentoo Foundation

require github [ user=${PN}-voip release=v${PV} suffix=tar.gz ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache \
    option-renames [ renames=[ 'speechd tts' ] ]

SUMMARY="Mumble is an open source, low-latency, high quality voice chat software"
DESCRIPTION="
Mumble is a voice chat application for groups. While it can be used for any kind of
activity, it is primarily intended for gaming. It can be compared to programs like Ventrilo or
TeamSpeak. People tend to simplify things, so when they talk about Mumble they either talk about
\"Mumble\" the client application or about \"Mumble & Murmur\" the whole voice chat application suite.
"
HOMEPAGE+=" https://${PN}.info"

LICENCES="BSD-3 MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    alsa
    avahi
    dbus
    jack
    pipewire
    pulseaudio
    tts [[ description = [ Support for text to speech ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        virtual/pkg-config
        x11-libs/qttools:5 [[ note = [ Qt5LinguistTools ] ]]
        x11-proto/xorgproto
    build+run:
        dev-libs/boost
        dev-libs/poco
        dev-libs/protobuf:=
        media-libs/libsndfile
        media-libs/opus[>=1.2.1]
        media-libs/rnnoise
        media-libs/speex[>=1.2_rc1]
        media-libs/speexdsp[>=1.2_rc1]
        x11-libs/libX11
        x11-libs/libXi
        x11-libs/qtbase:5[gui][sql][sqlite]
        x11-libs/qtsvg:5
        alsa? ( sys-sound/alsa-lib )
        avahi? ( net-dns/avahi[dns_sd] )
        jack? ( media-sound/jack-audio-connection-kit )
        pipewire? ( media/pipewire )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        pulseaudio? ( media-sound/pulseaudio )
        tts? ( app-speech/speechd )
        !media-sound/mumble [[
            description = [ media-sound/mumble was moved to ::desktop voip/mumble ]
            resolution = uninstall-blocked-before
        ]]
"

CMAKE_SOURCE=${WORKBASE}/${PNV}.src

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.4.230-cmake-FindPython3.patch
    "${FILES}"/d2838e27310b2b2b6505a2fc18900f2552815a76.patch
    "${FILES}"/f4cea62ed95e4967d8591f25e903f5e8fc2e2a30.patch
    "${FILES}"/${PN}-1.4.287-protobuf.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_NUMBER:STRING=$(ever range 3)
    -Dbundled-celt:BOOL=TRUE
    -Dbundled-opus:BOOL=FALSE
    -Dbundled-rnnoise:BOOL=FALSE
    -Dbundled-speex:BOOL=FALSE
    -Dbundle-qt-translations:BOOL=FALSE
    -Dclient:BOOL=TRUE
    -Ddebug-dependency-search:BOOL=FALSE
    -Ddisplay-install-paths:BOOL=TRUE
    -Dg15:BOOL=FALSE
    -Dmanual-plugin:BOOL=TRUE
    -Doverlay:BOOL=FALSE
    -Doverlay-xcompile:BOOL=FALSE
    -Dpackaging:BOOL=FALSE
    -Dplugin-callback-debug:BOOL=FALSE
    -Dplugin-debug:BOOL=FALSE
    -Dplugins:BOOL=FALSE
    -Dportaudio:BOOL=FALSE
    -Dqssldiffiehellmanparameters:BOOL=TRUE
    -Dqtspeech:BOOL=FALSE
    -Dretracted-plugins:BOOL=FALSE
    -Drnnoise:BOOL=TRUE
    -Dserver:BOOL=FALSE
    -Dstatic:BOOL=FALSE
    -Dtests:BOOL=FALSE
    -Dtranslations:BOOL=TRUE
    -Dupdate:BOOL=FALSE
    -Dwarnings-as-errors:BOOL=FALSE
    -Dxinput2:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    alsa
    'avahi zeroconf'
    dbus
    'jack jackaudio'
    pipewire
    pulseaudio
    'tts speechd'
)

src_install() {
    cmake_src_install

    insinto /usr/share/kservices5
    doins "${CMAKE_SOURCE}"/scripts/${PN}.protocol
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst

    elog "Mumble supports reading the kernel input devices, but may fall back to using the less optimal xinput2"
    elog "This can be solved with a simple udev rule: SUBSYSTEM==\"input\", GROUP=\"input\" MODE=\"660\""
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

