# Copyright 2013 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

require udev-rules

SUMMARY="Library for talking to devices with Qualcomm MSM Interface (QMI) protocol"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    mbim [[ description = [ QMI over MBIM, required by recent qualcomm modems ] ]]
    qrtr [[ description = [ Enable support for QRTR (qualcomm) protocol ] ]]
"

# Wants to access /dev/virtual/qmi*
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-apps/help2man
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.56]
        gnome-desktop/libgudev[>=232]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        mbim? ( net-libs/libmbim[>=1.18.0] )
        qrtr? ( net-libs/libqrtr-glib[>=1.0.0] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-collection=full
    --enable-firmware-update
    --enable-mm-runtime-check
    --enable-rmnet
    --disable-static
    --disable-Werror
    --with-udev
    --with-udev-base-dir="${UDEVDIR}"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'mbim mbim-qmux'
    'qrtr'
)

