# Copyright 2008-2011 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" new_download_scheme=true ]
require meson
require pam
require python [ blacklist=2 multibuild=false ]
require systemd-service test-dbus-daemon

SUMMARY="Policy framework for controlling privileges for system-wide services"
HOMEPAGE+=" https://www.freedesktop.org/wiki/Software/${PN}"

LICENCES="LGPL-2"
SLOT="1"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    duktape [[ description = [ Use Duktape instead of mozjs as javascript backend ] ]]
    gobject-introspection
    gtk-doc
    ( providers: systemd elogind consolekit ) [[
        *description = [ Login daemon provider ]
        number-selected = exactly-one
    ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.1.2 [[ description = [ Needed to build man-pages ] ]]
        app-text/docbook-xsl-stylesheets [[ description = [ Needed to build man-pages ] ]]
        dev-libs/libxslt[>=1.1.24]
        sys-apps/dbus [[ note = [ get dirs via pkgconfig ] ]]
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.3] )
    build+run:
        group/polkitd
        user/polkitd
        dev-libs/expat[>=2.0.1]
        dev-libs/glib:2[>=2.30.0]
        sys-libs/pam
        duktape? ( dev-libs/duktape[>=2.2.0] )
       !duktape? ( dev-libs/spidermonkey:102 )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.2] )
        providers:systemd? ( sys-apps/systemd )
        providers:elogind? ( sys-auth/elogind )
        providers:consolekit? ( sys-auth/ConsoleKit2 )
    test:
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/python-dbusmock[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/4b7a5c35fb3dd439e490f8fd6b1265d17c6d4bcb.patch
)

src_prepare() {
    meson_src_prepare

    # fix shebang
    edo sed -e 's:^#!.*:#!/usr/bin/env perl:' \
            -i src/polkitbackend/toarray.pl

    # Fix install location of polkit-agent-helper-1 and polkitd for us,
    # basically a revert of
    # https://gitlab.freedesktop.org/polkit/polkit/-/merge_requests/85
    # Related: https://gitlab.freedesktop.org/polkit/polkit/-/merge_requests/63
    edo sed -e "/pk_libprivdir =/s/ 'lib' / pk_libdir/" -i meson.build
}

src_configure() {
    local meson_params=(
        -Dauthfw=pam
        -Dexamples=false
        -Dlibs-only=false
        -Dman=true
        -Dos_type=gentoo
        -Dpam_module_dir=$(getpam_mod_dir)
        -Dpolkitd_user=polkitd
        -Dsystemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}

        $(meson_switch gobject-introspection introspection)
        $(meson_switch gtk-doc gtk_doc)

        $(option duktape -Djs_engine=duktape -Djs_engine=mozjs)

        $(expecting_tests -Dtests=true -Dtests=false)
    )

    if option providers:systemd ; then
        meson_params+=( -Dsession_tracking=libsystemd-login )
    elif option providers:elogind ; then
        meson_params+=( -Dsession_tracking=libelogind )
    else
        meson_params+=( -Dsession_tracking=ConsoleKit )
    fi

    exmeson "${meson_params[@]}"
}

src_test() {
    test-dbus-daemon_run-tests meson_src_test
}

src_install() {
    meson_src_install

    keepdir /etc/polkit-1/rules.d

    edo chown -R polkitd:root "${IMAGE}"{etc,usr/share}/polkit-1/rules.d
    edo chmod 0700 "${IMAGE}"/{etc,usr/share}/polkit-1/rules.d
}

